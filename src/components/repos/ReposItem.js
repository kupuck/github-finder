import React from 'react';
import PropTypes from 'prop-types';

const ReposItem = ({rep}) => {
  return (
    <div className="card">
      <h3>
        <a href={rep.html_url}>{rep.name}</a>
      </h3>
    </div>
  )
}
ReposItem.propTypes = {
  rep: PropTypes.object.isRequired
}

export default ReposItem