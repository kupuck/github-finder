import React from 'react';
import PropTypes from 'prop-types';
import ReposItem from './ReposItem';

const Repos = ({repos}) => {
  return repos.map(rep => <ReposItem rep={rep} key={rep.id} />)
}

Repos.propTypes = {
  repos: PropTypes.array.isRequired
}

export default Repos